import React from 'react';
import {
    View,
    TextInput,
    Image,
    StyleSheet
} from 'react-native';

import styleTextInput from './styles/SearchInput.js';

const SearchInput = () => {

    return (
        <View style={styleTextInput.bark}>
            <Image style={styleTextInput.img} resizeMode='contain' source={require('../assets/ic_search.png')} />
            <TextInput style={styleTextInput.textInput}
                placeholder='Search'
            />
        </View>
    );
}

export default SearchInput;
