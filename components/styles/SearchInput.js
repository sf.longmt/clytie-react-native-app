import {
    StyleSheet
} from 'react-native';

const styles = StyleSheet.create({

    bark: {
        paddingStart: 20,
        paddingEnd: 20,
        borderRadius: 20,
        backgroundColor: '#dfe6e9',
        height: 60,
        width: '100%',
        alignItems: 'center',
        flexDirection: 'row'
    },

    img: {
        height: 20,
        width: 20,

    },

    textInput: {
        marginStart: 5,
        color: '#636e72',
    }

});

export default styles;
